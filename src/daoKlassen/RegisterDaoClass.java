package daoKlassen;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import objektKlassen.Benutzer;

public class RegisterDaoClass {
	private static final String CLASSNAME = "org.sqlite.JDBC";
	private static final String CONNECTIONSTRING = "jdbc:sqlite:";
	private String path = null;

	public RegisterDaoClass () throws ClassNotFoundException {
		Class.forName(CLASSNAME);
		path = CONNECTIONSTRING + System.getProperty("user.dir") + "\\Hotelvergleich.db3";
	}
	//Erstellung eines neuen Benutzers in der Benutzer Tabelle
	public void insertUser(Benutzer benutzer) {
		Connection conn = null;
		PreparedStatement statement = null;
		try {
			conn = DriverManager.getConnection(path);
			String sql = "INSERT INTO Benutzer (Name, Vorname, Benutzername, Passwort, BenutzerArt) VALUES (?, ?, ?, ?, ?)";
			statement = conn.prepareStatement(sql);
			statement.setString(1, benutzer.getBenutzername());
			statement.setString(2, benutzer.getVorname());
			statement.setString(3, benutzer.getBenutzername());
			statement.setString(4, benutzer.getPasswort());
			statement.setString(5, benutzer.getBenutzerArt());
			statement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}



