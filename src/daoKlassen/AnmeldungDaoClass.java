package daoKlassen;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import objektKlassen.Benutzer;


public class AnmeldungDaoClass {

	private final String CLASSNAME = "org.sqlite.JDBC";
	private final String CONNECTIONSTRING = "jdbc:sqlite:";
	private String path = null;

	public AnmeldungDaoClass() throws ClassNotFoundException {
		Class.forName(CLASSNAME);
		path = CONNECTIONSTRING + System.getProperty("user.dir") + "\\Hotelvergleich.db3";
	}
	//Methode zum Anmelden
	public Benutzer selectBenutzer(String benutzername, String passwort) throws SQLException {
		Connection conn = null;
		PreparedStatement statement = null;
		Benutzer benutzer = null;
	//selektiert denn Benutzer raus mit den passenden eingegebenen werten
		conn = DriverManager.getConnection(path);
		String sql = "SELECT * FROM Benutzer WHERE benutzername = ? AND passwort = ?";
		statement = conn.prepareStatement(sql);
		statement.setString(1, benutzername);
		statement.setString(2, passwort);
		statement.executeQuery();

		ResultSet rs = statement.executeQuery();
		benutzer = new Benutzer(rs.getInt("BenutzerID"), rs.getString("name"), rs.getString("vorname"), rs.getString("benutzername"), rs.getString("passwort"), rs.getString("benutzerArt"));
		conn.close();

		return benutzer;
	}
	
	//Anzeige des Benutzers
	public Benutzer getBenutzer(int id) {
		Connection conn = null;
		PreparedStatement statement = null;
		Benutzer benutzer = null;

		try {
			conn = DriverManager.getConnection(path);
			String sql = "SELECT * FROM Benutzer WHERE BenutzerID = ?";
			statement = conn.prepareStatement(sql);
			statement.setInt(1, id);
			ResultSet rs = statement.executeQuery();
			benutzer = new Benutzer(rs.getInt("BenutzerID"), rs.getString("name"), rs.getString("vorname"), rs.getString("benutzername"), rs.getString("passwort"), rs.getString("benutzerArt"));

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}		
		return benutzer;
	}

}
