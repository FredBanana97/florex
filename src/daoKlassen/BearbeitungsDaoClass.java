package daoKlassen;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import objektKlassen.Anschrift;
import objektKlassen.Hotel;
import objektKlassen.Land;

public class BearbeitungsDaoClass {

	private final String CLASSNAME = "org.sqlite.JDBC";
	private final String CONNECTIONSTRING = "jdbc:sqlite:";
	private String path = null;

	public BearbeitungsDaoClass() throws ClassNotFoundException {
		Class.forName(CLASSNAME);
		//		path = CONNECTIONSTRING + this.getClass().getResource("Hotelvergleich.db3").getPath();
		path = CONNECTIONSTRING + System.getProperty("user.dir") + "\\Hotelvergleich.db3";
	}


	//Methode zur auswahl des Hotels
	public Hotel selectHotel(int hotelID) throws SQLException {
		Connection conn = null;
		PreparedStatement statement = null;
		Hotel hotel = null;

		try {
			conn = DriverManager.getConnection(path);
			String sql = "SELECT * FROM HOTEL WHERE (hotelID) = ?";
			statement = conn.prepareStatement(sql);
			statement.setInt(1, hotelID);
			statement.executeQuery();
			// Werte des Hotels werden �bergeben
			ResultSet rs = statement.executeQuery();
			if (rs.next()) {
				hotel = new Hotel(rs.getInt("hotelID"), rs.getString("hotelname"), rs.getInt("preisklasse"), rs.getInt("zimmer"), rs.getInt("bewertung"), rs.getInt("anzahlBetten"), rs.getDouble("preis"), rs.getInt("anschriftID"));
				return hotel;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	} 
	//Methode zum hinzuf�gen eines neues Hotels
	public void insertHotel(Hotel hotel) throws SQLException {
		Connection conn = null;
		PreparedStatement statement = null;

		try {
			conn = DriverManager.getConnection(path);
			String sql = "INSERT INTO HOTEL (hotelname, preisklasse, zimmer, bewertung, anzahlBetten, preis, anschriftID) VALUES (?, ?, ?, ?, ?, ?, ?)";
			statement = conn.prepareStatement(sql);
			statement.setString(1, hotel.getHotelname());
			statement.setInt(2, hotel.getPreisklasse());
			statement.setInt(3, hotel.getZimmer());
			statement.setInt(4, hotel.getBewertung());
			statement.setInt(5, hotel.getAnzahlBetten());
			statement.setDouble(6, hotel.getPreis());
			statement.setInt(7, hotel.getAnschriftID());
			statement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	//Anzeige der Anschrift/en des ausgew�hlten Hotels in einer Array List
	public Anschrift[] getAnschriftList() {
		ArrayList<Anschrift> anschriften = new ArrayList<Anschrift>();
		Connection conn = null;
		PreparedStatement statement = null;

		try {
			conn = DriverManager.getConnection(path);
			String sql = "SELECT * FROM ANSCHRIFT";
			statement = conn.prepareStatement(sql);
			statement.executeQuery();
			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				anschriften.add(new Anschrift(rs.getInt("AnschriftID"), rs.getString("Stra�e"), rs.getInt("hausnummer"), rs.getInt("Land_ID"), rs.getInt("ZipCode"), rs.getString("Telefon"), rs.getString("Email")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}		
		return anschriften.toArray(new Anschrift[anschriften.size()]);
	}
	//Anzeige der Anschrift/en des ausgew�hlten Hotels 
	public Anschrift getAnschrift(int id) {
		Connection conn = null;
		PreparedStatement statement = null;
		Anschrift anschrift = null;

		try {
			conn = DriverManager.getConnection(path);
			String sql = "SELECT * FROM ANSCHRIFT WHERE AnschriftID = ?";
			statement = conn.prepareStatement(sql);
			statement.setInt(1, id);
			ResultSet rs = statement.executeQuery();
			anschrift = new Anschrift(rs.getInt("AnschriftID"), rs.getString("Stra�e"), rs.getInt("hausnummer"), rs.getInt("Land_ID"), rs.getInt("ZipCode"), rs.getString("Telefon"), rs.getString("Email"));

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}		
		return anschrift;
	}

	//Anzeige des Landes des ausgew�hlten Hotels
	public Land getLand(int id) {
		Connection conn = null;
		PreparedStatement statement = null;
		Land land = null;

		try {
			conn = DriverManager.getConnection(path);
			String sql = "SELECT * FROM Land WHERE LandID = ?";
			statement = conn.prepareStatement(sql);
			statement.setInt(1, id);
			ResultSet rs = statement.executeQuery();
			land = new Land(rs.getInt("LandID"), rs.getString("country"), rs.getString("District"), rs.getString("City"));

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}		
		return land;
	}
	//Anzeige des Landes des ausgew�hlten Hotels in einer Array List
	public Land[] getLandList() {
		ArrayList<Land> laender = new ArrayList<Land>();
		Connection conn = null;
		PreparedStatement statement = null;

		try {
			conn = DriverManager.getConnection(path);
			String sql = "SELECT * FROM Land";
			statement = conn.prepareStatement(sql);
			statement.executeQuery();
			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				laender.add(new Land(rs.getInt("LandID"), rs.getString("country"), rs.getString("District"), rs.getString("City")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}		
		return laender.toArray(new Land[laender.size()]);
	}
	//Methode zum einf�gen einer neuen Anschrift
	public void insertAnschrift(Anschrift anschrift) {
		Connection conn = null;
		PreparedStatement statement = null;

		try {
			conn = DriverManager.getConnection(path);
			String sql = "INSERT INTO ANSCHRIFT (stra�e, hausnummer, land_ID, zipcode, telefon, email) VALUES (?, ?, ?, ?, ?, ?)";
			statement = conn.prepareStatement(sql);
			statement.setString(1, anschrift.getStra�e());
			statement.setInt(2, anschrift.getHausnummer());
			statement.setInt(3, anschrift.getLandID());
			statement.setInt(4, anschrift.getZipcode());
			statement.setString(5, anschrift.getTelefon());
			statement.setString(6, anschrift.getEmail());

			statement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
				statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	//Methode zum einf�gen eines neuen Landes
	public void insertLand(Land land) {
		Connection conn = null;
		PreparedStatement statement = null;

		try {
			conn = DriverManager.getConnection(path);
			String sql = "INSERT INTO LAND (country, district, city) VALUES (?, ?, ?)";
			statement = conn.prepareStatement(sql);
			statement.setString(1, land.getCountry());
			statement.setString(2, land.getDistrict());
			statement.setString(3, land.getCity());
			statement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	//Methode zum aktuellisieren des aktuellen Hotel Datensatzes
	public void updateHotel(Hotel hotel) {
		Connection conn = null;
		PreparedStatement statement = null;

		try {
			conn = DriverManager.getConnection(path);
			String sql = "UPDATE HOTEL SET hotelname=?, preisklasse=?, zimmer=?, bewertung=?, anzahlBetten=?, preis=?, anschriftID=? WHERE HotelID=?";
			statement = conn.prepareStatement(sql);
			statement.setString(1, hotel.getHotelname());
			statement.setInt(2, hotel.getPreisklasse());
			statement.setInt(3, hotel.getZimmer());
			statement.setInt(4, hotel.getBewertung());
			statement.setInt(5, hotel.getAnzahlBetten());
			statement.setDouble(6, hotel.getPreis());
			statement.setInt(7, hotel.getAnschriftID());
			statement.setInt(8, hotel.getHotelID());
			statement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	//Methode zum Anzeigen des aller ersten Datensatzes aus der Hoteltabelle
	public Hotel firstHotel() {
		Connection conn = null;
		PreparedStatement statement = null;
		Hotel hotel = null;

		try {
			conn = DriverManager.getConnection(path);
			String sql = "SELECT * FROM Hotel WHERE hotelID = (SELECT MIN(hotelID) from Hotel)";
			statement = conn.prepareStatement(sql);
			statement.executeQuery();

			ResultSet rs = statement.executeQuery();
			if (rs.next()) {
				hotel = new Hotel(rs.getInt("hotelID"), rs.getString("hotelname"), rs.getInt("preisklasse"), rs.getInt("zimmer"), rs.getInt("bewertung"), rs.getInt("anzahlBetten"), rs.getDouble("preis"), rs.getInt("anschriftID"));
				return hotel;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	} 
	//Methode zum Anzeigen des aller letzten Datensatzes aus der Hoteltabelle
	public Hotel lastHotel() {
		Connection conn = null;
		PreparedStatement statement = null;
		Hotel hotel = null;

		try {
			conn = DriverManager.getConnection(path);
			String sql = "SELECT * FROM Hotel WHERE hotelID = (SELECT MAX(hotelID) from Hotel)";
			statement = conn.prepareStatement(sql);
			statement.executeQuery();

			ResultSet rs = statement.executeQuery();
			if (rs.next()) {
				hotel = new Hotel(rs.getInt("hotelID"), rs.getString("hotelname"), rs.getInt("preisklasse"), rs.getInt("zimmer"), rs.getInt("bewertung"), rs.getInt("anzahlBetten"), rs.getDouble("preis"), rs.getInt("anschriftID"));
				return hotel;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	//Methode zum Anzeigen des n�chsten Datensatzes aus der Hoteltabelle
	public Hotel nextHotel(int id) {
		Connection conn = null;
		PreparedStatement statement = null;
		Hotel hotel = null;

		try {
			conn = DriverManager.getConnection(path);
			String sql = "SELECT * FROM HOTEL WHERE hotelID>? ORDER BY hotelID ASC";
			statement = conn.prepareStatement(sql);
			statement.setInt(1, id);
			statement.executeQuery();

			ResultSet rs = statement.executeQuery();
			if (rs.next()) {
				hotel = new Hotel(rs.getInt("hotelID"), rs.getString("hotelname"), rs.getInt("preisklasse"), rs.getInt("zimmer"), rs.getInt("bewertung"), rs.getInt("anzahlBetten"), rs.getDouble("preis"), rs.getInt("anschriftID"));
				return hotel;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	//Methode zum Anzeigen des letztens Datensatzes aus der Hoteltabelle
	public Hotel previousHotel(int id) {
		Connection conn = null;
		PreparedStatement statement = null;
		Hotel hotel = null;

		try {
			conn = DriverManager.getConnection(path);
			String sql = "SELECT * FROM HOTEL WHERE hotelID<? ORDER BY hotelID DESC";
			statement = conn.prepareStatement(sql);
			statement.setInt(1, id);
			statement.executeQuery();

			ResultSet rs = statement.executeQuery();
			if (rs.next()) {
				hotel = new Hotel(rs.getInt("hotelID"), rs.getString("hotelname"), rs.getInt("preisklasse"), rs.getInt("zimmer"), rs.getInt("bewertung"), rs.getInt("anzahlBetten"), rs.getDouble("preis"), rs.getInt("anschriftID"));
				return hotel;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	
	//Methode zum l�schen des aktuell ausgew�hlten Hotels
	public void deleteHotel(int id) {
		Connection conn = null;
		PreparedStatement statement = null;

		try {
			conn = DriverManager.getConnection(path);
			String sql = "DELETE FROM HOTEL WHERE HotelID=?";
			statement = conn.prepareStatement(sql);
			statement.setInt(1, id);
			statement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
