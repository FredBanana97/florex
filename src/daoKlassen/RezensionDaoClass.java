package daoKlassen;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import objektKlassen.Rezension;

public class RezensionDaoClass {

	private final String CLASSNAME = "org.sqlite.JDBC";
	private final String CONNECTIONSTRING = "jdbc:sqlite:";
	private String path = null;

	public RezensionDaoClass() throws ClassNotFoundException {
		Class.forName(CLASSNAME);
		//		path = CONNECTIONSTRING + this.getClass().getResource("Hotelvergleich.db3").getPath();
		path = CONNECTIONSTRING + System.getProperty("user.dir") + "\\Hotelvergleich.db3";
	}


	//Zum Speichern der Rezension
	public void insertRezension(Rezension rezi) throws SQLException {
		Connection conn = null;
		PreparedStatement statement = null;

		try {
			conn = DriverManager.getConnection(path);
			String sql = "INSERT INTO Rezension (HotelID, Titel, Beschreibung, BenutzerID) VALUES (?, ?, ?, ?)";
			statement = conn.prepareStatement(sql);
			statement.setInt(1, rezi.getHotelID());
			statement.setString(2, rezi.getTitel());
			statement.setString(3, rezi.getText());
			statement.setInt(4, rezi.getBenutzerId());
			statement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	//Methode zum Anzeigen des n�chsten Datensatzes aus der Rezensionstabelle
	public Rezension nextRezension(int id, int hotelId) {
		Connection conn = null;
		PreparedStatement statement = null;
		Rezension rezension = null;

		try {
			conn = DriverManager.getConnection(path);
			String sql = "SELECT * FROM Rezension WHERE rezID>? AND hotelID = ? ORDER BY rezID ASC";
			statement = conn.prepareStatement(sql);
			statement.setInt(1, id);
			statement.setInt(2, hotelId);
			statement.executeQuery();

			ResultSet rs = statement.executeQuery();
			if (rs.next()) {
				rezension = new Rezension(rs.getInt("rezID"), rs.getInt("HotelID"), rs.getString("Titel"), rs.getString("Beschreibung"), rs.getInt("BenutzerID"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return rezension;
	}

	//Methode zum Anzeigen des letztens Datensatzes aus der Rezensionstabelle
	public Rezension previousRezension(int id, int hotelId) {
		Connection conn = null;
		PreparedStatement statement = null;
		Rezension rezension = null;

		try {
			conn = DriverManager.getConnection(path);
			String sql = "SELECT * FROM Rezension WHERE rezID<? AND hotelID = ? ORDER BY rezID DESC";
			statement = conn.prepareStatement(sql);
			statement.setInt(1, id);
			statement.setInt(2, hotelId);
			statement.executeQuery();

			ResultSet rs = statement.executeQuery();
			if (rs.next()) {
				rezension = new Rezension(rs.getInt("rezID"), rs.getInt("HotelID"), rs.getString("Titel"), rs.getString("Beschreibung"), rs.getInt("BenutzerID"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return rezension;
	}
	
	//Methode zum Anzeigen des aller ersten Datensatzes aus der Rezensionstabelle
	public Rezension firstRezension(int hotelId) {
		Connection conn = null;
		PreparedStatement statement = null;
		Rezension rezension = null;

		try {
			conn = DriverManager.getConnection(path);
			String sql = "SELECT * FROM Rezension WHERE hotelID = ? AND rezID = (SELECT MIN(rezID) from Rezension WHERE hotelID = ?)";
			statement = conn.prepareStatement(sql);
			statement.setInt(1, hotelId);
			statement.setInt(2, hotelId);
			statement.executeQuery();

			ResultSet rs = statement.executeQuery();
			rezension = new Rezension(rs.getInt("rezID"), rs.getInt("HotelID"), rs.getString("Titel"), rs.getString("Beschreibung"), rs.getInt("BenutzerID"));
			
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Keine �bereinstimmung, bitte �berpr�fen Sie Ihre Eingabe.");
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return rezension;
	} 
}

