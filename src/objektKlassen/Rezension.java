package objektKlassen;

public class Rezension {
	
	private int rezId;
	private int hotelID;
	private String titel;
	private String text;
	private int benutzerId;
	
	public Rezension(int rezId, int hotelID, String titel, String text, int benutzerId) {
		super();
		this.rezId = rezId;
		this.hotelID = hotelID;
		this.titel = titel;
		this.text = text;
		this.benutzerId = benutzerId;
	}

	public int getRezId() {
		return rezId;
	}

	public void setRezId(int rezId) {
		this.rezId = rezId;
	}

	public int getHotelID() {
		return hotelID;
	}

	public void setHotelID(int hotelID) {
		this.hotelID = hotelID;
	}

	public String getTitel() {
		return titel;
	}

	public void setTitel(String titel) {
		this.titel = titel;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getBenutzerId() {
		return benutzerId;
	}

	public void setBenutzerId(int benutzerId) {
		this.benutzerId = benutzerId;
	}
	
}
