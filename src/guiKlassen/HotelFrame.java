package guiKlassen;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import daoKlassen.BearbeitungsDaoClass;
import daoKlassen.RezensionDaoClass;
import objektKlassen.Anschrift;
import objektKlassen.Benutzer;
import objektKlassen.Hotel;
import objektKlassen.Land;
import objektKlassen.Rezension;


public class HotelFrame extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextArea textAreaRezension;
	private JSeparator separator;
	private JLabel labelRezension;
	private JButton buttonZurueckR;
	private JButton buttonNaechste;
	private JLabel labelHotelBild;
	private JButton buttonBuchen;
	private JButton buttonStartseite;
	private JButton buttonRezension;
	private JTextField textFieldHotelname;
	private JTextField textFieldLand;
	private JTextField textFieldStadt;
	private JTextField textFieldBewertung;
	private JTextField textFieldPreis;
	private JTextField textFieldAnschrift;
	private JLabel labelHotelname;
	private JLabel labelLand;
	private JLabel labelStadt;
	private JLabel labelBewertung;
	private JLabel labelNewLabel;
	private JLabel labelNewLabel_2;
	private JButton buttonBearbeiten;
	private JLabel labelEmail;
	private JTextField textFieldEmail;
	private JLabel lblTelefon;
	private JTextField textFieldTelefon;
	private Hotel hotel;
	private Benutzer benutzer;
	private JScrollPane scrollPane;
	private JLabel lblTitleANzeige;
	private JLabel lblTitel;
	private JPanel panel;
	private Rezension currentRezension;
	private ImageIcon icon;

	public HotelFrame(Hotel hotel, Benutzer benutzer) {
		this.hotel = hotel;
		this.benutzer = benutzer;
		initGUI();
	}
	
	private void initGUI() {
		setResizable(false);
		setTitle("Florex - Hotel");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 455, 613);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		icon = new ImageIcon(HotelFrame.class.getResource("/guiKlassen/hotel.jpg"));

		panel = new JPanel();
		panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Rezension", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel.setBounds(10, 365, 260, 179);
		contentPane.add(panel);
		panel.setLayout(null);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(6, 53, 248, 117);
		panel.add(scrollPane);

		textAreaRezension = new JTextArea();
		textAreaRezension.setEditable(false);
		textAreaRezension.setWrapStyleWord(true);
		textAreaRezension.setLineWrap(true);
		scrollPane.setViewportView(textAreaRezension);
		textAreaRezension.setToolTipText("Von Urlaubern geschriebende Rezensionen.");
		textAreaRezension.setColumns(10);

		lblTitel = new JLabel("Titel:");
		lblTitel.setBounds(6, 16, 46, 20);
		panel.add(lblTitel);

		lblTitleANzeige = new JLabel("");
		lblTitleANzeige.setBounds(43, 16, 211, 20);
		panel.add(lblTitleANzeige);

		separator = new JSeparator();
		separator.setBounds(10, 314, 664, 2);
		contentPane.add(separator);

		labelRezension = new JLabel("Rezension");
		labelRezension.setFont(new Font("Tahoma", Font.PLAIN, 15));
		labelRezension.setBounds(101, 333, 76, 14);
		contentPane.add(labelRezension);

		buttonZurueckR = new JButton("Zur\u00FCck");
		buttonZurueckR.setToolTipText("Vorherige Rezension");
		buttonZurueckR.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				previousRezension();
			}
		});
		buttonZurueckR.setBounds(10, 327, 71, 31);
		contentPane.add(buttonZurueckR);

		buttonNaechste = new JButton("N\u00E4chste");
		buttonNaechste.setToolTipText("N\u00E4chste Rezension");
		buttonNaechste.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				nextRezension();
			}
		});
		buttonNaechste.setBounds(187, 327, 71, 31);
		contentPane.add(buttonNaechste);
		
		labelHotelBild = new JLabel("                    <Bild>");
		labelHotelBild.setIcon(icon);
		labelHotelBild.setBounds(240, 24, 180, 180);
		contentPane.add(labelHotelBild);

		buttonBuchen = new JButton("Buchen");
		buttonBuchen.setToolTipText("Aufenthalt buchen\r\n");
		buttonBuchen.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				BuchungsFrame frame = new BuchungsFrame(hotel, benutzer, icon);
				frame.setVisible(true);
				dispose();
			}
		});
		buttonBuchen.setBounds(286, 437, 151, 23);
		contentPane.add(buttonBuchen);

		buttonStartseite = new JButton("Startseite");
		buttonStartseite.setToolTipText("Zur\u00FCck zum Suchfenster\r\n");
		buttonStartseite.setBounds(286, 471, 151, 23);
		buttonStartseite.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				SuchFrame frame = new SuchFrame(benutzer);
				frame.setVisible(true);
				dispose();
			}
		});
		contentPane.add(buttonStartseite);

		buttonRezension = new JButton("Rezension Erstellen");
		buttonRezension.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				RezensionFrame frame = new RezensionFrame(hotel.getHotelID(), benutzer.getBenutzerId());
				frame.setVisible(true);
			}
		});
		buttonRezension.setToolTipText("Eigene Rezension erstellen");
		buttonRezension.setBounds(61, 550, 151, 23);
		contentPane.add(buttonRezension);

		textFieldHotelname = new JTextField();
		textFieldHotelname.setEditable(false);
		textFieldHotelname.setToolTipText("Name des Hotels.");
		textFieldHotelname.setBounds(91, 11, 122, 20);
		contentPane.add(textFieldHotelname);
		textFieldHotelname.setColumns(10);

		textFieldLand = new JTextField();
		textFieldLand.setEditable(false);
		textFieldLand.setToolTipText("Name des Landes wo das Hotel liegt.");
		textFieldLand.setBounds(91, 42, 122, 20);
		contentPane.add(textFieldLand);
		textFieldLand.setColumns(10);

		textFieldStadt = new JTextField();
		textFieldStadt.setEditable(false);
		textFieldStadt.setToolTipText("Name der Stadt wo das Hotel liegt.");
		textFieldStadt.setBounds(91, 73, 122, 20);
		contentPane.add(textFieldStadt);
		textFieldStadt.setColumns(10);

		textFieldBewertung = new JTextField();
		textFieldBewertung.setEditable(false);
		textFieldBewertung.setToolTipText("Durchschnittliche Bewertung des Hotels.");
		textFieldBewertung.setBounds(91, 104, 122, 20);
		contentPane.add(textFieldBewertung);
		textFieldBewertung.setColumns(10);

		textFieldPreis = new JTextField();
		textFieldPreis.setEditable(false);
		textFieldPreis.setToolTipText("Preis des Hotels.");
		textFieldPreis.setBounds(91, 135, 122, 20);
		contentPane.add(textFieldPreis);
		textFieldPreis.setColumns(10);

		textFieldAnschrift = new JTextField();
		textFieldAnschrift.setEditable(false);
		textFieldAnschrift.setToolTipText("Postleitzahl, Stra\u00DFe und Hausnummer.");
		textFieldAnschrift.setBounds(91, 223, 346, 20);
		contentPane.add(textFieldAnschrift);
		textFieldAnschrift.setColumns(10);

		labelHotelname = new JLabel("Hotelname:");
		labelHotelname.setBounds(10, 14, 71, 14);
		contentPane.add(labelHotelname);

		labelLand = new JLabel("Land:");
		labelLand.setBounds(10, 45, 46, 14);
		contentPane.add(labelLand);

		labelStadt = new JLabel("Stadt:");
		labelStadt.setBounds(10, 76, 46, 14);
		contentPane.add(labelStadt);

		labelBewertung = new JLabel("Bewertung:");
		labelBewertung.setBounds(10, 107, 71, 14);
		contentPane.add(labelBewertung);

		labelNewLabel = new JLabel("Preis:");
		labelNewLabel.setBounds(10, 138, 46, 14);
		contentPane.add(labelNewLabel);

		labelNewLabel_2 = new JLabel("Anschrift:");
		labelNewLabel_2.setBounds(10, 226, 71, 14);
		contentPane.add(labelNewLabel_2);

		buttonBearbeiten = new JButton("Bearbeiten");
		buttonBearbeiten.addActionListener(new ActionListener() {
			//�ffnet die barbeitungs frame
			@Override
			public void actionPerformed(ActionEvent e) {
				BearbeitungsFrame frame = new BearbeitungsFrame(hotel, benutzer);
				frame.setVisible(true);

			}
		});
		buttonBearbeiten.setToolTipText("Admin Funktion: Bearbeitung vom Datensatz");
		buttonBearbeiten.setBounds(286, 403, 151, 23);
		contentPane.add(buttonBearbeiten);

		labelEmail = new JLabel("Email:");
		labelEmail.setBounds(10, 196, 71, 14);
		contentPane.add(labelEmail);

		textFieldEmail = new JTextField();
		textFieldEmail.setToolTipText("Preis des Hotels.");
		textFieldEmail.setEditable(false);
		textFieldEmail.setColumns(10);
		textFieldEmail.setBounds(91, 193, 122, 20);
		contentPane.add(textFieldEmail);

		lblTelefon = new JLabel("Telefon:");
		lblTelefon.setBounds(10, 168, 71, 14);
		contentPane.add(lblTelefon);

		textFieldTelefon = new JTextField();
		textFieldTelefon.setToolTipText("Preis des Hotels.");
		textFieldTelefon.setEditable(false);
		textFieldTelefon.setColumns(10);
		textFieldTelefon.setBounds(91, 165, 122, 20);
		contentPane.add(textFieldTelefon);

		fillFields();
	}
// Zeigt die n�chste Rezension zu dem Hotel an
	private void nextRezension() {
		try {
			rezensionAnzeigen(new RezensionDaoClass().nextRezension(currentRezension.getRezId(), hotel.getHotelID()));
		} catch (ClassNotFoundException | NullPointerException e) {
			try {
				rezensionAnzeigen(new RezensionDaoClass().firstRezension(hotel.getHotelID()));
			} catch (ClassNotFoundException e1) {
				JOptionPane.showMessageDialog(null, "Keine �bereinstimmung, bitte �berpr�fen Sie Ihre Eingabe.");
			}
		}
	}
//Zeigt die vorherige Rezension an
	private void previousRezension() {
		try {
			rezensionAnzeigen(new RezensionDaoClass().previousRezension(currentRezension.getRezId(), hotel.getHotelID()));
		} catch (ClassNotFoundException | NullPointerException e) {
			try {
				rezensionAnzeigen(new RezensionDaoClass().firstRezension(hotel.getHotelID()));
			} catch (ClassNotFoundException e1) {
				JOptionPane.showMessageDialog(null, "Keine �bereinstimmung, bitte �berpr�fen Sie Ihre Eingabe.");
			}
		}
	}
//zeigt die n�chste rezension an
	private void rezensionAnzeigen(Rezension rezension) {
		try {
			currentRezension = rezension;
			lblTitleANzeige.setText(rezension.getTitel());
			textAreaRezension.setText(rezension.getText());
		} catch (NullPointerException e) {
			
		}
	}

	// F�llt die Felder mit den Werten des Hotels aus
	private void fillFields() {
		Anschrift anschrift = null;
		Land land = null;
		try {
			anschrift = new BearbeitungsDaoClass().getAnschrift(hotel.getAnschriftID());
			land = new BearbeitungsDaoClass().getLand(anschrift.getLandID());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		textFieldHotelname.setText(hotel.getHotelname());
		textFieldPreis.setText(hotel.getPreis() + "");
		textFieldBewertung.setText(hotel.getBewertung() + "");
		textFieldAnschrift.setText(anschrift.getStra�e() + " " + anschrift.getHausnummer() + " - " + anschrift.getZipcode());
		textFieldLand.setText(land.getCountry());
		textFieldStadt.setText(land.getCity());
		textFieldEmail.setText(anschrift.getEmail());
		textFieldTelefon.setText(anschrift.getTelefon());
	}
}
