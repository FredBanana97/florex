package objektKlassen;

public class Hotel {

	private int hotelID;
	private String hotelname;
	private int preisklasse;
	private int zimmer;
	private int bewertung;
	private int anzahlBetten;
	private double preis;
	private int anschriftID;

	public Hotel(int hotelID, String hotelname, int preisklasse, int zimmer, int bewertung, int anzahlBetten, double preis, int anschriftID) {
		super();
		this.hotelID = hotelID;
		this.hotelname = hotelname;
		this.preisklasse = preisklasse;
		this.zimmer = zimmer;
		this.bewertung = bewertung;
		this.anzahlBetten = anzahlBetten;
		this.preis = preis;
		this.anschriftID = anschriftID;
	}

	public int getHotelID() {
		return hotelID;
	}
	public void setHotelID(int hotelID) {
		this.hotelID = hotelID;
	}
	public String getHotelname() {
		return hotelname;
	}
	public void setHotelname(String hotelname) {
		this.hotelname = hotelname;
	}
	public int getPreisklasse() {
		return preisklasse;
	}
	public void setPreisklasse(int preisklasse) {
		this.preisklasse = preisklasse;
	}
	public int getZimmer() {
		return zimmer;
	}
	public void setZimmer(int zimmer) {
		this.zimmer = zimmer;
	}
	public int getBewertung() {
		return bewertung;
	}
	public void setBewertung(int bewertung) {
		this.bewertung = bewertung;
	}
	public int getAnzahlBetten() {
		return anzahlBetten;
	}
	public void setAnzahlBetten(int anzahlBetten) {
		this.anzahlBetten = anzahlBetten;
	}
	public double getPreis() {
		return preis;
	}
	public void setPreis(double preis) {
		this.preis = preis;
	}
	public int getAnschriftID() {
		return anschriftID;
	}
	public void setAnschriftID(int anschriftID) {
		this.anschriftID = anschriftID;
	}
}
