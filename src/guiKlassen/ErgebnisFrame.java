package guiKlassen;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

import daoKlassen.BearbeitungsDaoClass;
import objektKlassen.Benutzer;
import objektKlassen.Hotel;

public class ErgebnisFrame extends JFrame  {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JPanel panel;
	private JButton btnZurueck;
	private JScrollPane scrollPaneHotels;
	private JTable tableHotel;
	private DefaultTableModel tableModel;
	private JButton btnAuswaehlen;
	private Benutzer benutzer;

	public ErgebnisFrame(DefaultTableModel tableModel, Benutzer benutzer) {
		this.tableModel = tableModel;
		this.benutzer = benutzer;
		initGUI();
	}
	
	private void initGUI() {
		setTitle("Florex - Suchergebnisse");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1000, 353);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Top-Suchergebnisse", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(10, 11, 960, 246);
		contentPane.add(panel);
		panel.setLayout(null);

		scrollPaneHotels = new JScrollPane();
		scrollPaneHotels.setBounds(12, 30, 936, 203);
		panel.add(scrollPaneHotels);

		tableHotel = new JTable(tableModel);
		tableHotel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tableHotel.setSurrendersFocusOnKeystroke(true);
		scrollPaneHotels.setViewportView(tableHotel);

		btnZurueck = new JButton("Zur\u00FCck");
		btnZurueck.setToolTipText("Zur\u00FCck zur Suche");
		btnZurueck.setBounds(850, 270, 120, 23);
		btnZurueck.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				zurueck();
			}
		});
		contentPane.add(btnZurueck);

		btnAuswaehlen = new JButton("Ausw\u00E4hlen");
		btnAuswaehlen.setToolTipText("W\u00E4hlt das aktuell angeklickte Hotel aus");
		btnAuswaehlen.setBounds(10, 270, 120, 23);
		btnAuswaehlen.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				auswaehlen();
			}
		});
		contentPane.add(btnAuswaehlen);
	}
	//Zum �ffnen des gew�nschten Suchergebnisses
	private void auswaehlen() {
		int id = (int) tableHotel.getValueAt(tableHotel.getSelectedRow(), 0);
		try {
			Hotel hotel = new BearbeitungsDaoClass().selectHotel(id);
			HotelFrame frame = new HotelFrame(hotel, benutzer);
			frame.setVisible(true);
			dispose();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}
	
//Schlie�t die frame
	private void zurueck() {
		SuchFrame frame = new SuchFrame(benutzer);
		frame.setVisible(true);
		dispose();
	}
	
}
