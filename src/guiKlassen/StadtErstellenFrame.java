package guiKlassen;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import daoKlassen.BearbeitungsDaoClass;
import objektKlassen.Land;

public class StadtErstellenFrame extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textFieldStadt;
	private JTextField textFieldBundesland;
	private JTextField textFieldLand;
	private JPanel panel;
	private JButton btnSpeichern;
	private AnschriftErstellen adresseErstellenFrame;

	public StadtErstellenFrame(AnschriftErstellen adresseErstellenFrame) {
		setTitle("Florex - Stadt Erstellen");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 441, 190);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		this.adresseErstellenFrame = adresseErstellenFrame;
		
		panel = new JPanel();
		panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Stadt", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel.setBounds(10, 10, 407, 101);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblLand = new JLabel("Land:");
		lblLand.setBounds(6, 20, 46, 14);
		panel.add(lblLand);
		
		JLabel lblBundesland = new JLabel("Bundesland:");
		lblBundesland.setBounds(6, 45, 81, 14);
		panel.add(lblBundesland);
		
		JLabel lblStadt = new JLabel("Stadt:");
		lblStadt.setBounds(6, 70, 71, 14);
		panel.add(lblStadt);
		
		textFieldLand = new JTextField();
		textFieldLand.setBounds(85, 20, 314, 20);
		panel.add(textFieldLand);
		textFieldLand.setColumns(10);
		
		textFieldBundesland = new JTextField();
		textFieldBundesland.setBounds(85, 45, 314, 20);
		panel.add(textFieldBundesland);
		textFieldBundesland.setColumns(10);
		
		textFieldStadt = new JTextField();
		textFieldStadt.setBounds(85, 70, 314, 20);
		panel.add(textFieldStadt);
		textFieldStadt.setColumns(10);
		
		btnSpeichern = new JButton("Hinzuf\u00FCgen");
		btnSpeichern.setToolTipText("Werte als neues Land speichern");
		btnSpeichern.setBounds(10, 117, 407, 23);
		btnSpeichern.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				speichern();
			}
		});
		contentPane.add(btnSpeichern);
	}
	//Speichert das neue Land
	private void speichern() {
		try {
			Land land = new Land(0, textFieldLand.getText(), textFieldBundesland.getText(), textFieldStadt.getText());
			new BearbeitungsDaoClass().insertLand(land);
			adresseErstellenFrame.updateComboBox();
			dispose();
		} catch (NumberFormatException | ClassNotFoundException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, "Eingabe inkorrekt, bitte überprüfen.");
		}
	}
}
