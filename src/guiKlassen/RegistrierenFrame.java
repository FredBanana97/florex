package guiKlassen;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import daoKlassen.RegisterDaoClass;
import objektKlassen.Benutzer;

public class RegistrierenFrame extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JLabel labelVorname;
	private JLabel labelNachname;
	private JLabel labelPasswort;
	private JLabel labelPasswortBesttigen;
	private JLabel labelBenutzername;
	private JTextField textFieldVorname;
	private JTextField textFieldName;
	private JTextField textFieldBenutzername;
	private JButton buttonRegistrieren;
	private JButton buttonLoeschen;
	private JButton buttonZurueck;
	private JPasswordField passwordField;
	private JPasswordField passwordFieldRepeat;
	private JLabel labelIcon;
	private JSeparator separator;
	static RegistrierenFrame frame = new RegistrierenFrame();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public RegistrierenFrame() {
		initGUI();
	}
	private void initGUI() {
		setResizable(false);
		setTitle("Florex - Registrieren");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 220);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		labelVorname = new JLabel("Vorname:");
		labelVorname.setBounds(10, 11, 100, 14);
		contentPane.add(labelVorname);

		labelNachname = new JLabel("Name:");
		labelNachname.setBounds(10, 36, 70, 14);
		contentPane.add(labelNachname);


		labelBenutzername = new JLabel("Benutzername:");
		labelBenutzername.setBounds(10, 60, 145, 14);
		contentPane.add(labelBenutzername);

		labelPasswort = new JLabel("Passwort:");
		labelPasswort.setBounds(10, 85, 70, 14);
		contentPane.add(labelPasswort);

		labelPasswortBesttigen = new JLabel("Passwort best\u00E4tigen:");
		labelPasswortBesttigen.setBounds(10, 110, 140, 14);
		contentPane.add(labelPasswortBesttigen);

		textFieldVorname = new JTextField();
		textFieldVorname.setBounds(160, 10, 125, 20);
		contentPane.add(textFieldVorname);
		textFieldVorname.setColumns(10);

		textFieldName = new JTextField();
		textFieldName.setBounds(160, 35, 125, 20);
		contentPane.add(textFieldName);
		textFieldName.setColumns(10);

		textFieldBenutzername = new JTextField();
		textFieldBenutzername.setBounds(160, 60, 125, 20);
		contentPane.add(textFieldBenutzername);
		textFieldBenutzername.setColumns(10);

		passwordField = new JPasswordField();
		passwordField.setBounds(160, 85, 125, 20);
		contentPane.add(passwordField);

		passwordFieldRepeat = new JPasswordField();
		passwordFieldRepeat.setBounds(160, 110, 125, 20);
		contentPane.add(passwordFieldRepeat);

		buttonRegistrieren = new JButton("Registrieren");
		buttonRegistrieren.setToolTipText("Registriert die Daten als neuer Nutzer");
		buttonRegistrieren.setBounds(160, 148, 115, 23);
		buttonRegistrieren.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				registrieren();
			}
		});
		contentPane.add(buttonRegistrieren);

		buttonLoeschen = new JButton("L\u00F6schen");
		buttonLoeschen.setToolTipText("Felder zur\u00FCcksetzen");
		buttonLoeschen.setBounds(10, 148, 115, 23);
		buttonLoeschen.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				clearFields();				
			}
		});
		contentPane.add(buttonLoeschen);

		buttonZurueck = new JButton("Zur\u00FCck");
		buttonZurueck.setToolTipText("Schlie\u00DFt das Fenster");
		buttonZurueck.setBounds(299, 148, 125, 23);
		buttonZurueck.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				back();
			}
		});
		contentPane.add(buttonZurueck);

		labelIcon = new JLabel("");
		labelIcon.setIcon(new ImageIcon(RegistrierenFrame.class.getResource("/guiKlassen/icon_registrierung.png")));
		labelIcon.setBounds(324, 11, 100, 94);
		contentPane.add(labelIcon);

		separator = new JSeparator();
		separator.setBounds(10, 136, 414, 50);
		contentPane.add(separator);

	}
	
	private void back() {
		AnmeldungsFrame frame = new AnmeldungsFrame();
		frame.setVisible(true);
		dispose();
	}
	//Checkt ob das Passwort gr��er als 6 Zeichen ist
	@SuppressWarnings("deprecation")
	private boolean checkValidFields() {
		if (passwordField.getText().equals(passwordFieldRepeat.getText())
				&& passwordField.getText().length() >= 6
				&& textFieldVorname.getText().length() > 0
				&& textFieldName.getText().length() > 0
				&& textFieldBenutzername.getText().length() > 0) {
			return true;
		}
		return false;
	}
	//Legt neuen Datensatz mit den eingegeben Benutzerdaten an
	private void registrieren() {
		if (checkValidFields()) {
			@SuppressWarnings("deprecation")
			Benutzer benutzer = new Benutzer(0, textFieldName.getText(), textFieldVorname.getText(), textFieldBenutzername.getText(), passwordField.getText(), "User");
			try {
				new RegisterDaoClass().insertUser(benutzer);
				back();
			} catch (ClassNotFoundException | NullPointerException e) {
				e.printStackTrace();
			}			
		} else {
			JOptionPane.showMessageDialog(this, "Bitte �berpr�fen Sie Ihre Eingabe. (PW: min 6 Zeichen)");
		}
	}
	//Leert die Felder
	private void clearFields() {
		textFieldName.setText("");
		textFieldVorname.setText("");
		textFieldBenutzername.setText("");
		passwordField.setText("");
		passwordFieldRepeat.setText("");
	}

}
