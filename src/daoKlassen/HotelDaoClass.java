package daoKlassen;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import objektKlassen.Land;

public class HotelDaoClass {
	private static final String CLASSNAME = "org.sqlite.JDBC";
	private static final String CONNECTIONSTRING = "jdbc:sqlite:";
	private String path = null;

	public HotelDaoClass() {
		try {
			Class.forName(CLASSNAME);
			path = CONNECTIONSTRING + System.getProperty("user.dir") + "\\Hotelvergleich.db3";
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	//M�gliche Preisklassen
	public double[] getPreisklassen() {
		double[] preisklassen = {100, 300, 600, 1000, 1000};
		return preisklassen;
	}
	//Auslesung der Suchergebnisse
	public Object[][] getSuchergebniss(Land land, int bewertung, int preisklasse, int betten) {
		Object[][] data = null;
		List<List<Object>> masterList = new ArrayList<List<Object>>();
		Connection conn = null;
		PreparedStatement statement = null;
		//Join Befehl da auf mehrere Tabellen zugegriffen wird
		try {
			conn = DriverManager.getConnection(path);
			String sql = "SELECT h.HotelID, h.Preisklasse, h.Hotelname, h.Preis, h.AnzahlBetten, h.Zimmer, h.Bewertung, l.Country, l.District, l.City FROM Hotel h "
					+ "LEFT JOIN Anschrift a ON a.AnschriftID = h.AnschriftID "
					+ "LEFT JOIN Land l ON l.LandID = a.Land_ID "
					+ "WHERE Preisklasse = ? AND Bewertung = ? AND (AnzahlBetten = ?-1 OR AnzahlBetten = ? OR AnzahlBetten = ?+1)";
			statement = conn.prepareStatement(sql);
			statement.setInt(1, preisklasse);
			statement.setInt(2, bewertung);
			statement.setInt(3, betten);
			statement.setInt(4, betten);
			statement.setInt(5, betten);
			ResultSet rs = statement.executeQuery();
			
			while (rs.next()) {
				List<Object> list = new ArrayList<Object>();
				list.add(rs.getInt("HotelID"));
				list.add(rs.getString("Hotelname"));
				list.add(rs.getInt("Preisklasse"));
				list.add(rs.getInt("Zimmer"));
				list.add(rs.getInt("Bewertung"));
				list.add(rs.getInt("AnzahlBetten"));
				list.add(rs.getDouble("Preis"));
				list.add(rs.getString("Country"));
				list.add(rs.getString("District"));
				list.add(rs.getString("City"));
				masterList.add(list);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		//deklaration des Ergebnisses
		data = new Object[masterList.size()][masterList.get(0).size()];
		for (int i = 0; i < masterList.size(); i++) {
			for (int j = 0; j < masterList.get(0).size(); j++) {
				data[i][j] = masterList.get(i).get(j);
			}
		}
		
		return data;
	}

}
