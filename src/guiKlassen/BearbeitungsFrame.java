package guiKlassen;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.sql.SQLException;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import daoKlassen.BearbeitungsDaoClass;
import daoKlassen.HotelDaoClass;
import objektKlassen.Anschrift;
import objektKlassen.Benutzer;
import objektKlassen.Hotel;

public class BearbeitungsFrame extends JFrame  {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JPanel panel;
	private JLabel labelId;
	private JLabel labelHotelname;
	private JLabel labelPreis;
	private JTextField textFieldID;
	private JButton btnSuchen;
	private JTextField textFieldHotelname;
	private JTextField textFieldPreis;
	private JTextField textFieldZimmer;
	private JLabel labelZimmer;
	private JButton btnErsterEintrag;
	private JButton btnVorherigerEintrag;
	private JButton buttonNew;
	private JButton btnSichern;
	private JButton btnLoeschen;
	private JButton btnNaechsterEintrag;
	private JButton btnLetzterEintrag;
	private JLabel labelBewertung;
	private JLabel labelNewLabel;
	private JTextField textFieldBetten;
	private JLabel labelPreisklasse;
	private JButton btnZurueck;
	private JComboBox<Anschrift> comboBoxAnschriften;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JRadioButton rdbtnBewertung2;
	private JRadioButton rdbtnBewertung3;
	private JRadioButton rdbtnBewertung4;
	private JRadioButton rdbtnBewertung5;
	private JRadioButton rdbtnBewertung1;
	private JLabel lblPreisklasseValue;
	private Hotel initHotel;
	private Benutzer benutzer;

	public BearbeitungsFrame(Hotel hotel, Benutzer benutzer) {
		this.initHotel = hotel;
		this.benutzer = benutzer;
		initGUI();
	}
	
	private void initGUI() {
		setResizable(false);
		setTitle("Florex- Admin: Bearbeitung");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 551, 313);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		panel = new JPanel();
		panel.setLayout(null);
		panel.setBorder(new TitledBorder(null, "Werte", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(10, 11, 513, 215);
		contentPane.add(panel);

		labelId = new JLabel("ID:");
		labelId.setBounds(10, 20, 71, 20);
		panel.add(labelId);

		labelHotelname = new JLabel("Hotelname:");
		labelHotelname.setBounds(10, 45, 71, 20);
		panel.add(labelHotelname);

		labelPreis = new JLabel("Preis");
		labelPreis.setBounds(10, 70, 71, 20);
		panel.add(labelPreis);

		textFieldID = new JTextField();
		textFieldID.setColumns(10);
		textFieldID.setBounds(90, 20, 315, 20);
		panel.add(textFieldID);

		btnSuchen = new JButton("Suchen");
		btnSuchen.setToolTipText("Sucht das Hotel mit der eingegeben ID");
		btnSuchen.setBounds(413, 18, 89, 23);
		btnSuchen.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				suchen();
			}
		});
		panel.add(btnSuchen);

		textFieldHotelname = new JTextField();
		textFieldHotelname.setColumns(10);
		textFieldHotelname.setBounds(90, 45, 413, 20);
		panel.add(textFieldHotelname);

		textFieldPreis = new JTextField();
		textFieldPreis.setColumns(10);
		textFieldPreis.setBounds(90, 70, 413, 20);
		textFieldPreis.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
				if (textFieldPreis.getText().length() > 0) {
					preisklasseBerechnen();									
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
				if (textFieldPreis.getText().length() > 0) {
					preisklasseBerechnen();									
				}
			}

			@Override
			public void keyPressed(KeyEvent e) {
				if (textFieldPreis.getText().length() > 0) {
					preisklasseBerechnen();									
				}
			}
		});
		panel.add(textFieldPreis);

		textFieldZimmer = new JTextField();
		textFieldZimmer.setColumns(10);
		textFieldZimmer.setBounds(90, 95, 413, 20);
		panel.add(textFieldZimmer);

		labelZimmer = new JLabel("Zimmer:");
		labelZimmer.setBounds(10, 95, 71, 20);
		panel.add(labelZimmer);

		labelBewertung = new JLabel("Bewertung:");
		labelBewertung.setBounds(10, 155, 71, 20);
		panel.add(labelBewertung);

		labelNewLabel = new JLabel("Max Anzahl Betten:");
		labelNewLabel.setBounds(186, 180, 110, 20);
		panel.add(labelNewLabel);

		textFieldBetten = new JTextField();
		textFieldBetten.setBounds(300, 180, 85, 20);
		panel.add(textFieldBetten);
		textFieldBetten.setColumns(10);

		labelPreisklasse = new JLabel("Preisklasse:");
		labelPreisklasse.setBounds(10, 180, 71, 20);
		panel.add(labelPreisklasse);

		btnZurueck = new JButton("Zur\u00FCck");
		btnZurueck.setToolTipText("Zur\u00FCck zum Hotel");
		btnZurueck.setBounds(415, 182, 89, 23);
		btnZurueck.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				zurueck();
			}
		});
		// Schlie�t die GUI
		panel.add(btnZurueck);


		JLabel lblAnschrigt = new JLabel("Anschrift:");
		lblAnschrigt.setBounds(10, 125, 70, 20);
		panel.add(lblAnschrigt);

		comboBoxAnschriften = new JComboBox<Anschrift>();
		comboBoxAnschriften.setBounds(90, 123, 340, 23);
		panel.add(comboBoxAnschriften);

		JButton btnAddAdress = new JButton("+");
		btnAddAdress.setToolTipText("Leitet weiter zur Adresseerstellungsframe, falls die gew\u00FCnschte Anschrift fehlt");
		btnAddAdress.setFont(new Font("Arial", Font.PLAIN, 12));
		btnAddAdress.setBounds(440, 123, 63, 23);
		btnAddAdress.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				addAdress();
			}
		});
		//�bergibt die Werte aus der GUI an die DAO
		panel.add(btnAddAdress);

		rdbtnBewertung1 = new JRadioButton("1");
		rdbtnBewertung1.setSelected(true);
		buttonGroup.add(rdbtnBewertung1);
		rdbtnBewertung1.setBounds(90, 154, 40, 23);
		panel.add(rdbtnBewertung1);

		rdbtnBewertung2 = new JRadioButton("2");
		buttonGroup.add(rdbtnBewertung2);
		rdbtnBewertung2.setBounds(132, 154, 40, 23);
		panel.add(rdbtnBewertung2);

		rdbtnBewertung3 = new JRadioButton("3");
		buttonGroup.add(rdbtnBewertung3);
		rdbtnBewertung3.setBounds(174, 154, 40, 23);
		panel.add(rdbtnBewertung3);

		rdbtnBewertung4 = new JRadioButton("4");
		buttonGroup.add(rdbtnBewertung4);
		rdbtnBewertung4.setBounds(216, 154, 40, 23);
		panel.add(rdbtnBewertung4);

		rdbtnBewertung5 = new JRadioButton("5");
		buttonGroup.add(rdbtnBewertung5);
		rdbtnBewertung5.setBounds(258, 154, 40, 23);
		panel.add(rdbtnBewertung5);

		lblPreisklasseValue = new JLabel("");
		lblPreisklasseValue.setHorizontalAlignment(SwingConstants.CENTER);
		lblPreisklasseValue.setBounds(90, 180, 71, 20);
		panel.add(lblPreisklasseValue);

		btnVorherigerEintrag = new JButton("<<");
		btnVorherigerEintrag.setToolTipText("Zeigt das vorherige Hotel an");
		btnVorherigerEintrag.setBounds(87, 237, 56, 23);
		btnVorherigerEintrag.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				previous();
			}
		});
		//Geht auf denn vorherigen Datensatz
		contentPane.add(btnVorherigerEintrag);

		buttonNew = new JButton("Neu");
		buttonNew.setToolTipText("Neues Hotel erstellen");
		buttonNew.setBounds(153, 237, 71, 23);
		buttonNew.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				einf�gen();
			}
		});
		contentPane.add(buttonNew);

		btnSichern = new JButton("Sichern");
		btnSichern.setToolTipText("Speichert die \u00E4nderung");
		btnSichern.setBounds(234, 237, 71, 23);
		btnSichern.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				sichern();
			}
		});
		//Speichert die eingegebenen Werte
		contentPane.add(btnSichern);

		btnLoeschen = new JButton("L\u00F6schen");
		btnLoeschen.setToolTipText("L\u00F6scht das aktuell ausgew\u00E4hlte Hotel");
		btnLoeschen.setBounds(315, 237, 71, 23);
		btnLoeschen.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				delete();
			}
		});
		contentPane.add(btnLoeschen);

		btnNaechsterEintrag = new JButton(">>");
		btnNaechsterEintrag.setToolTipText("Zeigt das n\u00E4chste Hotel an");
		btnNaechsterEintrag.setBounds(396, 237, 56, 23);
		btnNaechsterEintrag.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				next();
			}
		});
		contentPane.add(btnNaechsterEintrag);

		btnLetzterEintrag = new JButton(">|");
		btnLetzterEintrag.setToolTipText("Springt auf das aller letzte Hotel");
		btnLetzterEintrag.setBounds(462, 237, 56, 23);
		btnLetzterEintrag.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				last();
			}
		});
		contentPane.add(btnLetzterEintrag);

		btnErsterEintrag = new JButton("|<");
		btnErsterEintrag.setToolTipText("Springt zur\u00FCck auf das aller erste Hotel");
		btnErsterEintrag.setBounds(20, 237, 56, 23);
		btnErsterEintrag.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				first();
			}
		});
		contentPane.add(btnErsterEintrag);

		updateComboBox();
	}
	//Schlie�t die Frame
	private void zurueck() {
		HotelFrame frame = new HotelFrame(initHotel, benutzer);
		frame.setVisible(true);
		dispose();
	}
	
	//L�scht denn aktuellen Datensatz
	private void delete() {
		try {
			new BearbeitungsDaoClass().deleteHotel(Integer.parseInt(textFieldID.getText()));
			JOptionPane.showMessageDialog(this, "Hotel mit der ID " + textFieldID.getText() + " wurde gel�scht.");
			clearFields();
		} catch (NumberFormatException | ClassNotFoundException e) {
			JOptionPane.showMessageDialog(this, "Es existiert kein Hotel mit dieser ID.");
		}
	}
	//Zeigt denn aller ersten Datensatz an
	private void first() {
		try {
			fillFields(new BearbeitungsDaoClass().firstHotel());
		} catch (ClassNotFoundException | NumberFormatException e) {
			e.printStackTrace();
		}
	}
//Zeigt den letzten Datensatz an
	private void last() {
		try {
			fillFields(new BearbeitungsDaoClass().lastHotel());
		} catch (ClassNotFoundException | NumberFormatException e) {
			e.printStackTrace();
		}
	}
//Anzeige des n�chsten Datensatzes
	private void next() {
		try {
			fillFields(new BearbeitungsDaoClass().nextHotel(Integer.parseInt(textFieldID.getText())));
		} catch (ClassNotFoundException | NumberFormatException e) {
			first();
		}
	}
//Anzeige des letzten Datesatzes
	private void previous() {
		try {
			fillFields(new BearbeitungsDaoClass().previousHotel(Integer.parseInt(textFieldID.getText())));
		} catch (ClassNotFoundException | NumberFormatException e) {
			last();
		}
	}
//Zum Speichern der eingegeben Werte
	private void sichern() {
		Anschrift anschrift = (Anschrift) comboBoxAnschriften.getSelectedItem();
		try {
			Hotel hotel = new Hotel(Integer.parseInt(textFieldID.getText()),
					textFieldHotelname.getText(),
					Integer.parseInt(lblPreisklasseValue.getText()),
					Integer.parseInt(textFieldZimmer.getText()),
					getSelectedRadioButton(),
					Integer.parseInt(textFieldBetten.getText()),
					Double.parseDouble(textFieldPreis.getText()),
					anschrift.getAnschriftId());
			new BearbeitungsDaoClass().updateHotel(hotel);
			clearFields();
		} catch (ClassNotFoundException | NumberFormatException e) {
			JOptionPane.showMessageDialog(this, "Bitte �berpr�fen Sie Ihre Eingabe.");
		}
	}
// Hinzuf�gen einer Adresse
	private void addAdress() {
		AnschriftErstellen frame = new AnschriftErstellen(this);
		frame.setVisible(true);
	}
//Einf�gen eines Hotels
	private void einf�gen() {
		Anschrift anschrift = (Anschrift) comboBoxAnschriften.getSelectedItem();
		try {
			Hotel hotel = new Hotel(0,
					textFieldHotelname.getText(),
					Integer.parseInt(lblPreisklasseValue.getText()),
					Integer.parseInt(textFieldZimmer.getText()),
					getSelectedRadioButton(),
					Integer.parseInt(textFieldBetten.getText()),
					Double.parseDouble(textFieldPreis.getText()),
					anschrift.getAnschriftId());
			new BearbeitungsDaoClass().insertHotel(hotel);
			clearFields();
		} catch (ClassNotFoundException | SQLException | NumberFormatException e) {
			JOptionPane.showMessageDialog(this, "Bitte �berpr�fen Sie Ihre Eingabe.");
		}
	}

	private int getSelectedRadioButton() {
		if (rdbtnBewertung1.isSelected()) {
			return 1;
		} else if (rdbtnBewertung2.isSelected()) {
			return 2;
		} else if (rdbtnBewertung3.isSelected()) {
			return 3;
		} else if (rdbtnBewertung4.isSelected()) {
			return 4;
		} else if (rdbtnBewertung5.isSelected()) {
			return 5;
		} else {
			return 0;
		}
	}

	private void setSelectedRadioButton(int index) {
		switch (index) {
		case 1:
			rdbtnBewertung1.setSelected(true);
			break;
		case 2:
			rdbtnBewertung2.setSelected(true);
			break;
		case 3:
			rdbtnBewertung3.setSelected(true);
			break;
		case 4:
			rdbtnBewertung4.setSelected(true);
			break;
		case 5:
			rdbtnBewertung5.setSelected(true);
			break;
		}
	}
//Berechenung der Preisklasse
	private void preisklasseBerechnen() {
		double[] preisklassen = new HotelDaoClass().getPreisklassen();
		try {
			double preis = Double.parseDouble(textFieldPreis.getText());
			if (preis < preisklassen[0]) {
				lblPreisklasseValue.setText("1");
			} else if (preis < preisklassen[1]) {
				lblPreisklasseValue.setText("2");
			} else if (preis < preisklassen[2]) {
				lblPreisklasseValue.setText("3");
			} else if (preis < preisklassen[3]) {
				lblPreisklasseValue.setText("4");
			} else if (preis >= preisklassen[4]) {
				lblPreisklasseValue.setText("5");
			}
		} catch (NumberFormatException e) {}
	}
//Suchen eines Hotels
	private void suchen() {
		Hotel hotel;
		try {
			hotel = new BearbeitungsDaoClass().selectHotel(Integer.parseInt(textFieldID.getText()));
			if (null != hotel) {
				fillFields(hotel);
			} else {
				JOptionPane.showMessageDialog(null, "Keine �bereinstimmung, bitte �berpr�fen Sie Ihre Eingabe.");
			}
		} catch (NumberFormatException | ClassNotFoundException | SQLException e) {
			JOptionPane.showMessageDialog(this, "Kein Hotel mit dieser ID gefunden.");
		}
	}

	public void updateComboBox() {
		try {
			comboBoxAnschriften.setModel(new DefaultComboBoxModel<Anschrift>(new BearbeitungsDaoClass().getAnschriftList()));
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	private void fillFields(Hotel hotel) {
		try {
			textFieldHotelname.setText(hotel.getHotelname());
			textFieldPreis.setText(String.valueOf(hotel.getPreisklasse()));
			textFieldZimmer.setText(String.valueOf(hotel.getZimmer()));
			setSelectedRadioButton(hotel.getBewertung());;
			textFieldPreis.setText(String.valueOf(hotel.getPreis()));
			textFieldBetten.setText(String.valueOf(hotel.getAnzahlBetten()));
			textFieldID.setText(String.valueOf(hotel.getHotelID()));
			preisklasseBerechnen();

			for (int i = 0; i < comboBoxAnschriften.getItemCount(); i++) {
				Anschrift anschrift = (Anschrift) comboBoxAnschriften.getItemAt(i);
				if (anschrift.getAnschriftId() == hotel.getAnschriftID()) {
					comboBoxAnschriften.setSelectedIndex(i);					
				}
			}
		} catch(NullPointerException e) {}
	}

	private void clearFields() {
		textFieldHotelname.setText("");
		textFieldPreis.setText("");
		textFieldZimmer.setText("");
		rdbtnBewertung1.setSelected(true);;
		textFieldPreis.setText("");
		textFieldBetten.setText("");
		textFieldID.setText("");
		lblPreisklasseValue.setText("");
		comboBoxAnschriften.setSelectedIndex(0);
	}
}
