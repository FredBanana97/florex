package objektKlassen;

public class Anschrift {
	
	
	private int anschriftId;
	private String strasse;
	private int hausnummer;
	private int landId;
	private int zipcode;
	private String telefon;
	private String email;
	
	public Anschrift(int anschriftId, String strasse, int hausnummer, int landId, int zipcode, String telefon, String email) {
		super();
		this.anschriftId = anschriftId;
		this.strasse = strasse;
		this.hausnummer = hausnummer;
		this.landId = landId;
		this.zipcode = zipcode;
		this.telefon = telefon;
		this.email = email;
	}
	
	public int getAnschriftId() {
		return anschriftId;
	}
	public void setAnschriftId(int anschriftId) {
		this.anschriftId = anschriftId;
	}
	public String getStra�e() {
		return strasse;
	}
	public void setStra�e(String stra�e) {
		this.strasse = stra�e;
	}
	public int getHausnummer() {
		return hausnummer;
	}
	public void setHausnummer(int hausnummer) {
		this.hausnummer = hausnummer;
	}
	public int getLandID() {
		return landId;
	}
	public void setLandID(int landId) {
		this.landId = landId;
	}
	public int getZipcode() {
		return zipcode;
	}
	public void setZipcode(int zipcode) {
		this.zipcode = zipcode;
	}
	public String getTelefon() {
		return telefon;
	}
	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	//Macht das,dass Strasse und Hausnummer als 1 String angezeigt werden
	@Override
	public String toString() {
		return strasse + " " + hausnummer;
	}

}
