package guiKlassen;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import daoKlassen.RezensionDaoClass;
import objektKlassen.Rezension;

public class RezensionFrame extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JLabel labelText1;
	private JLabel labelIhreBewertungIn;
	private JLabel labelDasTextfeldEin;
	private JButton buttonSpeichern;
	private JButton buttonZurck;
	private JTextArea textAreaRezension;
	private JLabel labelMaxZeichen;
	private JScrollPane scrollPane;
	private int hotelId, benutzerId;
	private JTextField textFieldTitel;

	public RezensionFrame(int hotelId, int benutzerId) {
		this.hotelId = hotelId;
		this.benutzerId = benutzerId;
		initGUI();
	}
	
	private void initGUI() {
		setTitle("Florex - Rezension");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 530, 251);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		labelText1 = new JLabel("Bitte geben sie");
		labelText1.setBounds(10, 11, 173, 14);
		contentPane.add(labelText1);

		labelIhreBewertungIn = new JLabel("Ihre Bewertung in");
		labelIhreBewertungIn.setBounds(10, 25, 173, 14);
		contentPane.add(labelIhreBewertungIn);

		labelDasTextfeldEin = new JLabel("das Textfeld ein.");
		labelDasTextfeldEin.setBounds(10, 39, 168, 14);
		contentPane.add(labelDasTextfeldEin);

		buttonSpeichern = new JButton("Speichern");
		buttonSpeichern.setToolTipText("Speichert die verfasste Rezension in der Datenbank ab, damit diese vom Hotel abgerufen werden kann");
		buttonSpeichern.setBounds(10, 64, 159, 23);
		buttonSpeichern.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				sichern();
			}
		});
		contentPane.add(buttonSpeichern);

		buttonZurck = new JButton("Zur\u00FCck");
		buttonZurck.setToolTipText("Zur\u00FCck zum Hotel");
		buttonZurck.setBounds(10, 98, 159, 23);
		buttonZurck.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		contentPane.add(buttonZurck);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(179, 36, 325, 154);
		contentPane.add(scrollPane);

		textAreaRezension = new JTextArea();
		textAreaRezension.setLineWrap(true);
		textAreaRezension.setWrapStyleWord(true);
		textAreaRezension.setColumns(10);
		textAreaRezension.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {
				checkLength(e);
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				checkLength(e);
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				checkLength(e);
			}
		});
		scrollPane.setViewportView(textAreaRezension);

		labelMaxZeichen = new JLabel("max. 180 Zeichen");
		labelMaxZeichen.setBounds(388, 187, 126, 14);
		contentPane.add(labelMaxZeichen);
		
		textFieldTitel = new JTextField();
		textFieldTitel.setBounds(219, 8, 285, 20);
		contentPane.add(textFieldTitel);
		textFieldTitel.setColumns(10);
		
		JLabel lblTitel = new JLabel("Titel:");
		lblTitel.setBounds(179, 8, 45, 20);
		contentPane.add(lblTitel);

	}
	
	//Counted die restlichen Zeichen die man �brig hat
	private void checkLength(KeyEvent e) {
		if (textAreaRezension.getText().length() > 180) {
			textAreaRezension.setText(textAreaRezension.getText().substring(0, 180));
			e.consume();
		}
		labelMaxZeichen.setText("Noch " + (180 - textAreaRezension.getText().length()) + " Zeichen.");
	}
	
	//Methode zum sichern der neu angelegten Rezension
	private void sichern() {
		try {
			Rezension rez = new Rezension(0, hotelId, textFieldTitel.getText(), textAreaRezension.getText(), benutzerId);
			new RezensionDaoClass().insertRezension(rez);
			dispose();
		} catch (ClassNotFoundException | NumberFormatException | SQLException e) {
			JOptionPane.showMessageDialog(this, "Fehler! Bitte �berpr�fen Sie Ihre Eingabe.");
		}
	}
}
