package guiKlassen;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JSpinner.DefaultEditor;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

import daoKlassen.BearbeitungsDaoClass;
import daoKlassen.HotelDaoClass;
import objektKlassen.Benutzer;
import objektKlassen.Land;

public class SuchFrame extends JFrame  {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JPanel panel;
	private JLabel labelUrlaubsland;
	private JLabel lblBewertung;
	private JLabel lblBettten;
	private JButton btnSuchen;
	private JButton btnNeu;
	private JComboBox<Land> comboBoxOrt;
	private JLabel lblPreisklasse;
	private JRadioButton rdbtnP1;
	private JRadioButton rdbtnP5;
	private JRadioButton rdbtnP4;
	private JRadioButton rdbtnP3;
	private JRadioButton rdbtnP2;
	private JLabel lblPreisklasseAnzeige;
	private JRadioButton rdbtnB1;
	private JRadioButton rdbtnB5;
	private JRadioButton rdbtnB2;
	private JRadioButton rdbtnB3;
	private JRadioButton rdbtnB4;
	private JLabel lblBewertungAnzeige;
	private JSpinner spinnerBetten;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private final ButtonGroup buttonGroup_1 = new ButtonGroup();
	private double[] preisklassen = new HotelDaoClass().getPreisklassen();
	private Benutzer benutzer;

	public SuchFrame(Benutzer benutzer) {
		this.benutzer = benutzer;
		initGUI();
	}

	private void initGUI() {
		setTitle("Florex - Hotelsuche");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 460, 245);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Such Kriterien", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(12, 11, 420, 140);
		contentPane.add(panel);
		panel.setLayout(null);

		labelUrlaubsland = new JLabel("Ort:");
		labelUrlaubsland.setBounds(10, 25, 91, 20);
		panel.add(labelUrlaubsland);

		lblPreisklasse = new JLabel("Preisklasse:");
		lblPreisklasse.setBounds(10, 50, 81, 20);
		panel.add(lblPreisklasse);

		lblBewertung = new JLabel("Bewertung:");
		lblBewertung.setBounds(10, 75, 81, 20);
		panel.add(lblBewertung);

		lblBettten = new JLabel("Betten:");
		lblBettten.setBounds(10, 100, 81, 20);
		panel.add(lblBettten);

		lblPreisklasseAnzeige = new JLabel("Bis: " + preisklassen[0] + " �");
		lblPreisklasseAnzeige.setBounds(329, 50, 91, 20);
		panel.add(lblPreisklasseAnzeige);

		lblBewertungAnzeige = new JLabel("Sterne");
		lblBewertungAnzeige.setBounds(329, 75, 74, 20);
		panel.add(lblBewertungAnzeige);

		comboBoxOrt = new JComboBox<Land>();
		comboBoxOrt.setBounds(113, 24, 208, 23);
		try {
			comboBoxOrt.setModel(new DefaultComboBoxModel<>(new BearbeitungsDaoClass().getLandList()));
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}
		panel.add(comboBoxOrt);

		rdbtnP1 = new JRadioButton("1");
		buttonGroup.add(rdbtnP1);
		rdbtnP1.setSelected(true);
		rdbtnP1.setBounds(113, 50, 40, 23);
		rdbtnP1.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				setPreisklasseLbl("Bis: " + preisklassen[0]);
			}
		});
		panel.add(rdbtnP1);

		rdbtnP2 = new JRadioButton("2");
		buttonGroup.add(rdbtnP2);
		rdbtnP2.setBounds(155, 50, 40, 23);
		rdbtnP2.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				setPreisklasseLbl("Bis: " + preisklassen[1]);
			}
		});
		panel.add(rdbtnP2);

		rdbtnP3 = new JRadioButton("3");
		buttonGroup.add(rdbtnP3);
		rdbtnP3.setBounds(197, 50, 40, 23);
		rdbtnP3.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				setPreisklasseLbl("Bis: " + preisklassen[2]);
			}
		});
		panel.add(rdbtnP3);

		rdbtnP4 = new JRadioButton("4");
		buttonGroup.add(rdbtnP4);
		rdbtnP4.setBounds(239, 50, 40, 23);
		rdbtnP4.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				setPreisklasseLbl("Bis: " + preisklassen[3]);
			}
		});
		panel.add(rdbtnP4);

		rdbtnP5 = new JRadioButton("5");
		buttonGroup.add(rdbtnP5);
		rdbtnP5.setBounds(281, 50, 40, 23);
		rdbtnP5.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				setPreisklasseLbl("�ber: " + preisklassen[4]);
			}
		});
		panel.add(rdbtnP5);

		rdbtnB1 = new JRadioButton("1");
		buttonGroup_1.add(rdbtnB1);
		rdbtnB1.setSelected(true);
		rdbtnB1.setBounds(113, 75, 40, 23);
		panel.add(rdbtnB1);

		rdbtnB2 = new JRadioButton("2");
		buttonGroup_1.add(rdbtnB2);
		rdbtnB2.setBounds(155, 75, 40, 23);
		panel.add(rdbtnB2);

		rdbtnB3 = new JRadioButton("3");
		buttonGroup_1.add(rdbtnB3);
		rdbtnB3.setBounds(197, 75, 40, 23);
		panel.add(rdbtnB3);

		rdbtnB4 = new JRadioButton("4");
		buttonGroup_1.add(rdbtnB4);
		rdbtnB4.setBounds(239, 75, 40, 23);
		panel.add(rdbtnB4);

		rdbtnB5 = new JRadioButton("5");
		buttonGroup_1.add(rdbtnB5);
		rdbtnB5.setBounds(281, 75, 40, 23);
		panel.add(rdbtnB5);

		spinnerBetten = new JSpinner();
		spinnerBetten.setModel(new SpinnerNumberModel(1, 1, 10, 1));
		spinnerBetten.setBounds(113, 100, 208, 22);
		((DefaultEditor) spinnerBetten.getEditor()).getTextField().setEditable(false);
		panel.add(spinnerBetten);

		btnSuchen = new JButton("Suchen");
		btnSuchen.setToolTipText("Sucht die passenden Hotels mit den ausgew\u00E4hlten Werten");
		btnSuchen.setBounds(270, 159, 155, 23);
		btnSuchen.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				suchen();
			}
		});
		contentPane.add(btnSuchen);

		btnNeu = new JButton("Neue Suche");
		btnNeu.setToolTipText("Setzt alles zur\u00FCck");
		btnNeu.setBounds(12, 159, 155, 23);
		btnNeu.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				clearFields();
			}
		});
		contentPane.add(btnNeu);
	}
	
	private void setPreisklasseLbl(String value) {
		lblPreisklasseAnzeige.setText(value + " �");
	}
	
	private int getSelectedRadioButtonBewertung() {
		if (rdbtnB1.isSelected()) {
			return 1;
		} else if (rdbtnB2.isSelected()) {
			return 2;
		} else if (rdbtnB3.isSelected()) {
			return 3;
		} else if (rdbtnB4.isSelected()) {
			return 4;
		} else if (rdbtnB5.isSelected()) {
			return 5;
		} else {
			return 0;
		}
	}
	
	private int getSelectedRadioButtonPreisklasse() {
		if (rdbtnP1.isSelected()) {
			return 1;
		} else if (rdbtnP2.isSelected()) {
			return 2;
		} else if (rdbtnP3.isSelected()) {
			return 3;
		} else if (rdbtnP4.isSelected()) {
			return 4;
		} else if (rdbtnP5.isSelected()) {
			return 5;
		} else {
			return 0;
		}
	}
	
	private void suchen() {
		try {
			String[] columnNames = {"HotelID", "Hotelname", "Preisklasse", "Zimmer", "Sterne", "Anzahl Betten pro Zimmer", "Preis", "Land", "Bundesland", "Stadt"};
			Land land = (Land) comboBoxOrt.getSelectedItem();
			Object[][] data = new HotelDaoClass().getSuchergebniss(land, getSelectedRadioButtonBewertung(), getSelectedRadioButtonPreisklasse(), Integer.parseInt(spinnerBetten.getValue().toString()));
			DefaultTableModel model = new DefaultTableModel(data, columnNames);
			ErgebnisFrame frame = new ErgebnisFrame(model, benutzer);
			frame.setVisible(true);
			dispose();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, "Keine Hotels mit diesen Spezifikationen gefunden.");
		}
	}
	
	private void clearFields() {
		rdbtnP1.setSelected(true);
		rdbtnB1.setSelected(true);
		comboBoxOrt.setSelectedIndex(0);
		spinnerBetten.setValue(1);
		setPreisklasseLbl("Bis: " + preisklassen[0]);
	}
}
