package guiKlassen;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import objektKlassen.Benutzer;
import objektKlassen.Hotel;

public class BuchungsFrame extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JPanel panel;
	private JLabel labelVon;
	private JTextField textFieldVon;
	private JLabel labelBis;
	private JTextField textFieldBis;
	private JLabel labelAnzahlPersonen;
	private JTextField textFieldPersonen;
	private JLabel labelBild;
	private JSeparator separator;
	private JLabel labelBrutto;
	private JLabel labelMw;
	private JLabel labelNettopreis;
	private JPanel panel_1;
	private JTextField textFieldBrutto;
	private JTextField textFieldSteuer;
	private JTextField textFieldNetto;
	private JSeparator separator_1;
	private JLabel labelHotelname;
	private JTextField textFieldName;
	private JButton buttonZurueck;
	private JButton buttonBuchen;
	private Hotel hotel;
	private Benutzer benutzer;
	private Calendar calVon = Calendar.getInstance();
	private Calendar calBis = Calendar.getInstance();
	private int differenz = 0;
	private double netto;
	private double steuer;
	private double brutto;
	private JLabel labelFormatDdmmyy;
	private NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.GERMANY);
	private ImageIcon icon;

	public BuchungsFrame(Hotel hotel, Benutzer benutzer, ImageIcon icon) {
		this.hotel = hotel;
		this.benutzer = benutzer;
		this.icon = icon;
		initGUI();
	}

	private void initGUI() {
		setResizable(false);
		setTitle("Florex - Buchen");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 360);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		panel = new JPanel();
		panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Zeitraum", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(10, 39, 220, 90);
		contentPane.add(panel);
		panel.setLayout(null);

		labelVon = new JLabel("Von:");
		labelVon.setBounds(10, 21, 59, 14);
		panel.add(labelVon);

		textFieldVon = new JTextField();
		textFieldVon.setHorizontalAlignment(SwingConstants.CENTER);
		textFieldVon.setBounds(79, 18, 131, 20);
		textFieldVon.setColumns(10);
		textFieldVon.addKeyListener(getKeyListener(textFieldVon));
		panel.add(textFieldVon);

		labelBis = new JLabel("Bis:");
		labelBis.setBounds(10, 46, 59, 14);
		panel.add(labelBis);

		textFieldBis = new JTextField();
		textFieldBis.setHorizontalAlignment(SwingConstants.CENTER);
		textFieldBis.setBounds(79, 43, 131, 20);
		textFieldBis.setColumns(10);
		textFieldBis.addKeyListener(getKeyListener(textFieldBis));
		panel.add(textFieldBis);

		labelFormatDdmmyy = new JLabel("Format: DD.MM.YY");
		labelFormatDdmmyy.setHorizontalAlignment(SwingConstants.CENTER);
		labelFormatDdmmyy.setBounds(79, 67, 131, 14);
		panel.add(labelFormatDdmmyy);


		labelAnzahlPersonen = new JLabel("Anzahl Personen:");
		labelAnzahlPersonen.setBounds(10, 155, 122, 14);
		contentPane.add(labelAnzahlPersonen);

		textFieldPersonen = new JTextField();
		textFieldPersonen.setBounds(133, 152, 97, 20);
		textFieldPersonen.setColumns(10);
		textFieldPersonen.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
				calculate();
			}

			@Override
			public void keyReleased(KeyEvent e) {
				calculate();
			}

			@Override
			public void keyPressed(KeyEvent e) {
				calculate();
			}
		});
		contentPane.add(textFieldPersonen);

		labelBild = new JLabel();
		labelBild.setBounds(254, 8, 180, 180);
		labelBild.setIcon(icon);
		contentPane.add(labelBild);

		separator = new JSeparator();
		separator.setBounds(10, 193, 424, 2);
		contentPane.add(separator);

		panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(null, "Preis", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.setBounds(10, 206, 250, 116);
		contentPane.add(panel_1);
		panel_1.setLayout(null);

		labelBrutto = new JLabel("Bruttopreis:");
		labelBrutto.setBounds(10, 91, 114, 14);
		panel_1.add(labelBrutto);

		labelMw = new JLabel("Mwst 19%:");
		labelMw.setBounds(10, 46, 114, 14);
		panel_1.add(labelMw);

		labelNettopreis = new JLabel("Nettopreis:");
		labelNettopreis.setBounds(10, 21, 114, 14);
		panel_1.add(labelNettopreis);

		textFieldBrutto = new JTextField();
		textFieldBrutto.setHorizontalAlignment(SwingConstants.RIGHT);
		textFieldBrutto.setEditable(false);
		textFieldBrutto.setColumns(10);
		textFieldBrutto.setBounds(122, 88, 97, 20);
		panel_1.add(textFieldBrutto);

		textFieldSteuer = new JTextField();
		textFieldSteuer.setHorizontalAlignment(SwingConstants.RIGHT);
		textFieldSteuer.setEditable(false);
		textFieldSteuer.setColumns(10);
		textFieldSteuer.setBounds(122, 43, 97, 20);
		panel_1.add(textFieldSteuer);

		textFieldNetto = new JTextField();
		textFieldNetto.setHorizontalAlignment(SwingConstants.RIGHT);
		textFieldNetto.setEditable(false);
		textFieldNetto.setColumns(10);
		textFieldNetto.setBounds(122, 18, 97, 20);
		panel_1.add(textFieldNetto);

		separator_1 = new JSeparator();
		separator_1.setBounds(10, 78, 230, 2);
		panel_1.add(separator_1);

		labelHotelname = new JLabel("Hotelname:");
		labelHotelname.setBounds(10, 14, 76, 14);
		contentPane.add(labelHotelname);

		textFieldName = new JTextField(hotel.getHotelname());
		textFieldName.setHorizontalAlignment(SwingConstants.CENTER);
		textFieldName.setEditable(false);
		textFieldName.setBounds(82, 11, 146, 20);
		textFieldName.setColumns(10);
		contentPane.add(textFieldName);

		buttonZurueck = new JButton("Zur\u00FCck");
		buttonZurueck.setToolTipText("Zur\u00FCck zum Hotel");
		buttonZurueck.setBounds(270, 237, 164, 23);
		buttonZurueck.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				HotelFrame frame = new HotelFrame(hotel, benutzer);
				frame.setVisible(true);
				dispose();
			}
		});
		contentPane.add(buttonZurueck);

		buttonBuchen = new JButton("Buchen");
		buttonBuchen.setToolTipText("Gibt den Buchungsbeleg in der Konsole aus.");
		buttonBuchen.setBounds(270, 271, 164, 23);
		buttonBuchen.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				buchen();
			}
		});
		contentPane.add(buttonBuchen);
	}

	private void buchen() {
		if (brutto > 0) {
			System.out.println("########################################");
			System.out.println("      --- Buchungbest�tigung ---      ");
			System.out.println("                                      ");
			System.out.println("       Hotel          : " + hotel.getHotelname());
			System.out.println("       Preis p. Nacht : " + hotel.getPreis());
			System.out.println("       Betten         : " + textFieldPersonen.getText());
			System.out.println("");
			System.out.println("       Netto          : " + nf.format(netto));
			System.out.println("       Mwst.          : " + nf.format(steuer));
			System.out.println("       Brutto         : " + nf.format(brutto));
			System.out.println("########################################");
		}
	}

	private KeyListener getKeyListener(final JTextField textField) {
		return new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {}

			@Override
			public void keyReleased(KeyEvent e) {
				//Wenn mehr als 8 zeichen im textfeld sind ignoriere tastenanschlag au�er backspace zum l�schen
				if (textField.getText().length() >= 8 && e.getKeyCode() != KeyEvent.VK_BACK_SPACE && e.getKeyCode() != KeyEvent.VK_LEFT && e.getKeyCode() != KeyEvent.VK_RIGHT) {
					e.consume();
				}
				calculate();
			}

			@Override
			public void keyPressed(KeyEvent e) {}
		};
	}

	private void calculate() {
		double nix = 0;
		if (textFieldVon.getText().length() == 8 && textFieldBis.getText().length() == 8) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yy");
			try {
				Date dateV = sdf.parse(textFieldVon.getText());
				Date dateB = sdf.parse(textFieldBis.getText());

				calVon.setTime(dateV);
				calBis.setTime(dateB);
				Calendar today = Calendar.getInstance();

				differenz = calBis.get(Calendar.DAY_OF_YEAR) - calVon.get(Calendar.DAY_OF_YEAR);

				netto = hotel.getPreis() * Integer.parseInt(textFieldPersonen.getText()) * differenz;
				steuer = netto/100 * 19;
				brutto = netto + steuer;
				
				int year = calVon.get(Calendar.YEAR);     
				int dayV = Integer.parseInt(textFieldVon.getText().substring(0, 2));
				int dayB = Integer.parseInt(textFieldBis.getText().substring(0, 2));
				int monV = Integer.parseInt(textFieldVon.getText().substring(3, 5));
				int monB = Integer.parseInt(textFieldBis.getText().substring(3, 5));

				if (!isDateValid(dayV, monV, year) || !isDateValid(dayB, monB, year) || netto < 0  || (calVon.get(Calendar.DAY_OF_YEAR) < today.get(Calendar.DAY_OF_YEAR) || calBis.get(Calendar.DAY_OF_YEAR) < today.get(Calendar.DAY_OF_YEAR))) {
					JOptionPane.showMessageDialog(null, "Ung�ltige Eingabe! Bitte geben Sie g�ltige Werte an.");
					return;					
				}
				
				textFieldBrutto.setText(nf.format(brutto));
				textFieldNetto.setText(nf.format(netto));
				textFieldSteuer.setText(nf.format(steuer));
			} catch (Exception e1) {
				textFieldBrutto.setText(nf.format(nix));
				textFieldNetto.setText(nf.format(nix));
				textFieldSteuer.setText(nf.format(nix));
			}
		} else {
			textFieldBrutto.setText(nf.format(nix));
			textFieldNetto.setText(nf.format(nix));
			textFieldSteuer.setText(nf.format(nix));
		}
	}
	
	//Methode zur �berpr�fung, ob ein Jahr ein Schaltjahr und ob das eingegeben Datum g�ltig izt.
	private boolean isDateValid(int day, int month, int year) {
		if (day <= 31) {
			if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
				if (day <= 31) {
					return true;
				}
			} else if (month == 2) {
				boolean isLeapYear = ((year % 4 == 0) && (year % 100 != 0) || (year % 400 == 0));
				if (isLeapYear) {
					if (day <= 29) {
						return true;
					}
				} else {
					if (day <= 28) {
						return true;
					}
				}
			} else if (month == 4 || month == 6 || month == 9 || month == 11) {
				if (day <= 30) {
					return true;
				}
			}
		}
		return false;
	}
}