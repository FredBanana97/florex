package objektKlassen;

public class Land {

	private int landId;
	private String country;
	private String district;
	private String city;
	
	public Land(int landId, String country, String district, String city) {
		super();
		this.landId = landId;
		this.country = country;
		this.district = district;
		this.city = city;
	}
	
	public int getLandId() {
		return landId;
	}
	public void setLandId(int landId) {
		this.landId = landId;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
	//Macht das ,dass Land und die Stadt als 1 String angezeigt werden
	@Override
	public String toString() {
		return city + " - " + country;
	}
	
}
