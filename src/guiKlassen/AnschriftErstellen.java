package guiKlassen;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import daoKlassen.BearbeitungsDaoClass;
import objektKlassen.Anschrift;
import javax.swing.JComboBox;
import objektKlassen.Land;
import java.awt.Font;

public class AnschriftErstellen extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textFieldEmail;
	private JTextField textFieldTelefon;
	private JTextField textFieldPostleitzahl;
	private JTextField textFieldHausnummer;
	private JTextField textFieldStrasse;
	private JPanel panel;
	private JButton btnSpeichern;
	private BearbeitungsFrame bearbeitungsFrame;
	private JComboBox<Land> comboBoxOrt;

	public AnschriftErstellen(BearbeitungsFrame bearbeitungsFrame) {
		setTitle("Florex - Anschrift Erstellen");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 441, 259);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		this.bearbeitungsFrame = bearbeitungsFrame;
		
		panel = new JPanel();
		panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Anschrift", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel.setBounds(10, 10, 407, 177);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblStrasse = new JLabel("Stra\u00DFe:");
		lblStrasse.setBounds(6, 20, 46, 14);
		panel.add(lblStrasse);
		
		JLabel lblHausnummer = new JLabel("Hausnr.:");
		lblHausnummer.setBounds(6, 45, 81, 14);
		panel.add(lblHausnummer);
		
		JLabel lblOrt = new JLabel("Ort:");
		lblOrt.setBounds(6, 70, 71, 14);
		panel.add(lblOrt);
		
		JLabel lblZip = new JLabel("Postleitzahl:");
		lblZip.setBounds(6, 95, 69, 14);
		panel.add(lblZip);
		
		JLabel lblTelefon = new JLabel("Telefon:");
		lblTelefon.setBounds(6, 120, 71, 14);
		panel.add(lblTelefon);
		
		JLabel lblEmail = new JLabel("Email:");
		lblEmail.setBounds(6, 145, 46, 14);
		panel.add(lblEmail);
		
		textFieldStrasse = new JTextField();
		textFieldStrasse.setBounds(85, 20, 314, 20);
		panel.add(textFieldStrasse);
		textFieldStrasse.setColumns(10);
		
		textFieldHausnummer = new JTextField();
		textFieldHausnummer.setBounds(85, 45, 314, 20);
		panel.add(textFieldHausnummer);
		textFieldHausnummer.setColumns(10);
		
		textFieldPostleitzahl = new JTextField();
		textFieldPostleitzahl.setBounds(85, 95, 314, 20);
		panel.add(textFieldPostleitzahl);
		textFieldPostleitzahl.setColumns(10);
		
		textFieldTelefon = new JTextField();
		textFieldTelefon.setBounds(85, 120, 314, 20);
		panel.add(textFieldTelefon);
		textFieldTelefon.setColumns(10);
		
		textFieldEmail = new JTextField();
		textFieldEmail.setBounds(85, 145, 314, 20);
		panel.add(textFieldEmail);
		textFieldEmail.setColumns(10);
		
		comboBoxOrt = new JComboBox<Land>();
		comboBoxOrt.setBounds(85, 70, 237, 20);
		panel.add(comboBoxOrt);
		
		JButton btnAddStadt = new JButton("+");
		btnAddStadt.setToolTipText("Leitet weiter zur Landerstellungsframe, falls das gew\u00FCnschte Land fehlt");
		btnAddStadt.setFont(new Font("Arial", Font.PLAIN, 12));
		btnAddStadt.setBounds(336, 70, 63, 20);
		btnAddStadt.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				addLand();
			}
		});
		panel.add(btnAddStadt);
		
		btnSpeichern = new JButton("Hinzuf\u00FCgen");
		btnSpeichern.setToolTipText("Speichert die Gerade erstellte Anschrift");
		btnSpeichern.setBounds(10, 191, 407, 23);
		btnSpeichern.addActionListener(new ActionListener() {
			//Methode zum ausf�hren der Speichern methode
			@Override
			public void actionPerformed(ActionEvent e) {
				speichern();
			}
		});
		contentPane.add(btnSpeichern);
		
		updateComboBox();;
	}
	//�ffnet die LandErstellenFrame
	private void addLand() {
		StadtErstellenFrame frame = new StadtErstellenFrame(this);
		frame.setVisible(true);
	}
	//Zum Aktuellisieren der neuen L�nder
	public void updateComboBox() {
		try {
			comboBoxOrt.setModel(new DefaultComboBoxModel<Land>(new BearbeitungsDaoClass().getLandList()));
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	//Speichert die neu erstelle Anschrift 
	private void speichern() {
		Land land = (Land) comboBoxOrt.getSelectedItem();
		try {
			Anschrift anschrift = new Anschrift(0, textFieldStrasse.getText(),
					Integer.parseInt(textFieldHausnummer.getText().trim()),
					land.getLandId(),
					Integer.parseInt(textFieldPostleitzahl.getText().trim()),
					textFieldTelefon.getText().trim(),
					textFieldEmail.getText());
			new BearbeitungsDaoClass().insertAnschrift(anschrift);
			bearbeitungsFrame.updateComboBox();
			dispose();
		} catch (NumberFormatException | ClassNotFoundException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, "Eingabe inkorrekt, bitte �berpr�fen.");
		}
	}
}
