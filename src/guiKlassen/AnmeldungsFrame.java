package guiKlassen;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import daoKlassen.AnmeldungDaoClass;
import objektKlassen.Benutzer;

//hauptframe
public class AnmeldungsFrame extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textFieldBenutzername;
	private JPasswordField passwordFieldPasswort;
	private JLabel lblName;
	private JLabel lblPasswort;
	private JSeparator separator;
	private JButton Registrieren;
	static AnmeldungsFrame frame = new AnmeldungsFrame();
	private JButton Anmeldung;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public AnmeldungsFrame() {
		initGUI();
	}
	private void initGUI() {
		setResizable(false);
		setTitle("Florex - Anmeldung");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 310, 160);
		contentPane = new JPanel();
		contentPane.setBackground(SystemColor.menu);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);


		textFieldBenutzername = new JTextField();
		textFieldBenutzername.setBackground(Color.WHITE);
		textFieldBenutzername.setBounds(110, 10, 182, 20);
		textFieldBenutzername.setColumns(10);
		textFieldBenutzername.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {}
			
			@Override
			public void keyReleased(KeyEvent e) {}
			
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == 10) {
					anmelden();
				}
			}
			
		});
		contentPane.add(textFieldBenutzername);

		passwordFieldPasswort = new JPasswordField();
		passwordFieldPasswort.setBackground(Color.WHITE);
		passwordFieldPasswort.setBounds(110, 43, 182, 20);
		passwordFieldPasswort.setColumns(10);
		passwordFieldPasswort.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {}
			
			@Override
			public void keyReleased(KeyEvent e) {}
			
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == 10) {
					anmelden();
				}
			}
			
		});
		contentPane.add(passwordFieldPasswort);

		Anmeldung = new JButton("Anmelden");
		Anmeldung.setToolTipText("Meldet den Nutzer an");
		Anmeldung.setBackground(Color.LIGHT_GRAY);
		Anmeldung.setBounds(10, 91, 101, 23);
		Anmeldung.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				anmelden();
			}
		});
		contentPane.add(Anmeldung);

		Registrieren = new JButton("Registieren");
		Registrieren.setToolTipText("Neuen Nutzer anlegen");
		Registrieren.setBackground(Color.LIGHT_GRAY);
		Registrieren.setBounds(192, 91, 100, 23);
		Registrieren.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				registrieren();
			}
		});
		contentPane.add(Registrieren);

		lblName = new JLabel("Benutzername:");
		lblName.setBounds(10, 14, 101, 14);
		contentPane.add(lblName);

		lblPasswort = new JLabel("Passwort:");
		lblPasswort.setBounds(10, 46, 90, 14);
		contentPane.add(lblPasswort);

		separator = new JSeparator();
		separator.setBounds(10, 76, 282, 2);
		contentPane.add(separator);
	}

	@SuppressWarnings("deprecation")
	//�bergabe der eingegeben Daten an die Dao
	private void anmelden() {
		try {
			Benutzer benutzer = new AnmeldungDaoClass().selectBenutzer(textFieldBenutzername.getText(), passwordFieldPasswort.getText());
			SuchFrame sFrame = new SuchFrame(benutzer);
			sFrame.setVisible(true);				
			dispose();
		} catch (ClassNotFoundException | SQLException e1) {
			JOptionPane.showMessageDialog(null, "Keine �bereinstimmung, bitte �berpr�fen Sie Ihre Eingabe.");
		}
	}
	//�ffnung der registrierungs frame
	private void registrieren() {
		RegistrierenFrame frame = new RegistrierenFrame();
		frame.setVisible(true);
		dispose();
	}
}
