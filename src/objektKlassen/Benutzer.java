package objektKlassen;

public class Benutzer {
	
	private int benutzerId;	
	private String name;
	private String vorname;
	private String benutzername;
	private String passwort;
	private String benutzerArt;

	public Benutzer(int benutzerId, String name, String vorname, String benutzername, String passwort, String benutzerArt) {
		super();
		this.benutzerId = benutzerId;
		this.name = name;
		this.vorname = vorname;
		this.passwort = passwort;
		this.benutzerArt = benutzerArt;
		this.benutzername = benutzername;
	}

	public int getBenutzerId() {
		return benutzerId;
	}
	public void setBenutzerId(int benutzerId) {
		this.benutzerId = benutzerId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getVorname() {
		return vorname;
	}
	public void setVorname(String vorname) {
		this.vorname = vorname;
	}
	public String getPasswort() {
		return passwort;
	}
	public void setPasswort(String passwort) {
		this.passwort = passwort;
	}
	public String getBenutzerArt() {
		return benutzerArt;
	}
	public void setBenutzerArt(String benutzerArt) {
		this.benutzerArt = benutzerArt;
	}
	public String getBenutzername() {
		return benutzername;
	}

	public void setBenutzername(String benutzername) {
		this.benutzername = benutzername;
	}
}
